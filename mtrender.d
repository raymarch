module mtrender;

//version = render_debug;


private:
import gfxcore : texImage, vlWidth, vlHeight, scale, scanlines;
import raytracer;
import vecs;

import core.time : MonoTime, Duration;

import core.atomic;
import core.sync.condition;
import core.sync.rwmutex;
import core.sync.mutex;
import core.thread;

import std.concurrency;


__gshared Mutex mutexCondCanRender;
__gshared Condition waitCondCanRender;
shared bool renderComplete = true;
shared int renderDie = 0; // 1: waiting for death; 2: dead
shared uint frameDur;


public @property bool mtrenderComplete () {
  return atomicLoad(renderComplete);
}


public @property uint mtrenderFrameTime () {
  return atomicLoad(frameDur);
}


public void mtrenderStartSignal () {
  atomicStore(renderComplete, false);
  synchronized(mutexCondCanRender) waitCondCanRender.notify();
}


public void mtrenderInit () {
  mutexCondCanRender = new Mutex();
  waitCondCanRender = new Condition(mutexCondCanRender);

  nextLineAdjustment = texImage.adjustmentForNextLine();
  offR = texImage.redByteOffset();
  offB = texImage.blueByteOffset();
  offG = texImage.greenByteOffset();
  bpp = texImage.bytesPerPixel();
  imgdata = texImage.getDataPointer();
  fpxoffset = texImage.offsetForTopLeftPixel();

  { import core.stdc.stdio; printf("%u tiles per image\n", tilesPerTexture()); }

  initLights();

  animate();
  prepareRenderer();
  foreach (uint tile; 0..tilesPerTexture) renderToTexture(tile);

  renderThread = new Thread(&renderThreadFunc);
  renderThread.start();
}


public void mtrenderShutdown () {
  synchronized(mutexCondCanRender) waitCondCanRender.notify(); // just in case
  atomicStore(renderDie, 1); // die
  while (atomicLoad(renderDie) != 2) {}
}


// ////////////////////////////////////////////////////////////////////////// //
enum TileSize = 32; // tile is 32x32 pixels

// various image parameters
__gshared int nextLineAdjustment;
__gshared int offR;
__gshared int offB;
__gshared int offG;
__gshared int bpp;
__gshared ubyte* imgdata;
__gshared int fpxoffset;

__gshared Vec3[4] scp;
__gshared LightInfo[] lights;

__gshared Vec3 grady1v;
__gshared Vec3 grady2v;

__gshared Thread renderThread;


// ////////////////////////////////////////////////////////////////////////// //
void prepareRenderer () @nogc {
  import std.math : sin, cos;

  // vertex shader common part
  auto vpn = (vrp-prp).normalize;
  auto u = (vuv%vpn).normalize;
  auto v = vpn%u;
  auto vcv = prp+vpn;

  //vertex shader for each vertex
  immutable RmFloat[2][4] vPos = [
    [-1,  1], //0--1
    [ 1,  1], //|  |
    [ 1, -1], //3--2
    [-1, -1],
  ];

  Vec3 scrCoord = void; // temp
  immutable RmFloat cxy = cast(RmFloat)vlWidth/cast(RmFloat)vlHeight;
  foreach (immutable i; 0..vPos.length) {
    scrCoord.x = vcv.x+vPos.ptr[i].ptr[0]*u.x*cxy+vPos.ptr[i].ptr[1]*v.x;
    scrCoord.y = vcv.y+vPos.ptr[i].ptr[0]*u.y*cxy+vPos.ptr[i].ptr[1]*v.y;
    scrCoord.z = vcv.z+vPos.ptr[i].ptr[0]*u.z*cxy+vPos.ptr[i].ptr[1]*v.z;
    scp.ptr[i] = (scrCoord-prp).normalize;
  }

  RmFloat y_inc = rmFloat!1.0/vlHeight;

  grady1v = (scp.ptr[3]-scp.ptr[0])*y_inc;
  grady2v = (scp.ptr[2]-scp.ptr[1])*y_inc;
}


uint tilesPerTexture () @nogc {
  return ((vlWidth+TileSize-1)/TileSize)*((vlHeight+TileSize-1)/TileSize);
}


void tileOfs (uint tile, out int x, out int y) @nogc {
  immutable tpx = (vlWidth+TileSize-1)/TileSize;
  x = (tile%tpx)*TileSize;
  y = (tile/tpx)*TileSize;
}


// Adam don't care about @nogc, but image functions actually are
void renderToTexture (uint tile) @nogc {
  RmFloat x = -rmFloat!0.5, y = -rmFloat!0.5, x_inc = rmFloat!1.0/vlWidth, y_inc = rmFloat!1.0/vlHeight;

  int tofsx, tofsy;
  tileOfs(tile, tofsx, tofsy);
  //{ import core.stdc.stdio; printf("tile=%u; x=%u; y=%u\n", tile, tofsx, tofsy); }
  int ex = tofsx+TileSize;
  int ey = tofsy+TileSize;
  if (ex > vlWidth) ex = vlWidth;
  if (ey > vlHeight) ey = vlHeight;

  x += x_inc*tofsx;
  y += y_inc*tofsy;

  auto accy1v = scp.ptr[0]+grady1v*tofsy;
  auto accy2v = scp.ptr[1]+grady2v*tofsy;

  Vec3 colorout = void;
  Vec3 gradxv = void, gradxvdown = void, accxvdown = void, accxv = void, rdx = void;

  auto offset = fpxoffset+tofsy*scale*nextLineAdjustment+tofsx*scale*bpp;
  auto startOfLine = imgdata+offset; // get our pointer lined up on the first pixel

  RmFloat oldx = x;
  foreach (int iy; tofsy..ey) {
    auto vbptr = startOfLine; // we keep the start of line separately so moving to the next line is simple and portable
    startOfLine += nextLineAdjustment*scale;
    gradxv = (accy2v-accy1v)*x_inc;
    accxv = accy1v+gradxv*tofsx;

    accy1v += grady1v;
    accy2v += grady2v;

    accxvdown = accy1v+gradxv*tofsx;
    gradxvdown = (accy2v-accy1v)*x_inc;

    foreach (int ix; tofsx..ex) {
      rdx = accxv+gradxv;
      raymarch(/*x, y,*/ accxv, lights[], colorout, rdx, accxvdown);
      ubyte r = void, g = void, b = void;
      if (colorout.x < 0 || colorout.y < 0 || colorout.z < 0) {
        r = g = b = 0;
      } else {
        r = clampToByte(cast(int)(colorout.x*rmFloat!255.0));
        g = clampToByte(cast(int)(colorout.y*rmFloat!255.0));
        b = clampToByte(cast(int)(colorout.z*rmFloat!255.0));
      }
      static if (scale > 1) {
        foreach (immutable _; 0..scale) {
          vbptr[offR] = r;
          vbptr[offG] = g;
          vbptr[offB] = b;
          static if (!scanlines) {
            auto vbptrv = vbptr;
            foreach (immutable _1; 1..scale) {
              vbptrv += nextLineAdjustment;
              vbptrv[offR] = r;
              vbptrv[offG] = g;
              vbptrv[offB] = b;
            }
          }
          vbptr += bpp;
        }
      } else {
        vbptr[offR] = r;
        vbptr[offG] = g;
        vbptr[offB] = b;
        vbptr += bpp;
      }
      x += x_inc;
      //accxv += gradxv;
      accxv = rdx;
      accxvdown += gradxvdown;
    }
    y += y_inc;
    //x = -rmFloat!0.5;
    x = oldx;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared MonoTime sttime, lasttime;
shared bool paused = false;
__gshared wasPaused = false;


public void mtrenderTogglePause () {
  auto ps = atomicLoad(paused);
  atomicStore(paused, !paused);
}


public void mtrenderPaused (bool v) {
  atomicStore(paused, v);
}


void initLights () {
  lights.length = 3;
  // fog
  lights[0].active = true;
  lights[0].color = Vec3(rmFloat!0.8, rmFloat!0.9, rmFloat!1.0);
  // light
  lights[1].active = true;
  lights[1].color = Vec3(rmFloat!1.00, rmFloat!0.85, rmFloat!0.55);
  // light
  lights[2].active = true;
  lights[2].color = Vec3(rmFloat!0.00, rmFloat!1.0, rmFloat!0.0);
}


void animate () @nogc {
  __gshared bool firstTime = true;
  if (firstTime) {
    sttime = lasttime = MonoTime.currTime;
    firstTime = false;
  }

  import std.math : abs, sin, cos;

  if (atomicLoad(paused)) {
    if (!wasPaused) {
      wasPaused = true;
    }
    lasttime = MonoTime.currTime;
  } else {
    wasPaused = false;
    //worldtime = cast(RmFloat)(MonoTime.currTime-sttime).total!"msecs"/rmFloat!1000.0;
    auto time = MonoTime.currTime;
    worldtime += cast(RmFloat)(time-lasttime).total!"msecs"/rmFloat!1000.0;
    lasttime = time;
  }

  Vec3 auto_vuv = void, auto_vrp = void, auto_prp = void;

  // view up vector
  auto_vuv.x = 0; //sin(worldtime/*timemsecs*/);
  auto_vuv.y = 1;
  auto_vuv.z = 0;

  // view reference point
  auto_vrp.x = 0; //sin(time*rmFloat!0.7)*rmFloat!10.0;
  auto_vrp.y = 0;
  auto_vrp.z = 0; //cos(time*rmFloat!0.9)*rmFloat!10.0;

  // camera position
  auto_prp.x = rmFloat!3.0; //sin(time*rmFloat!0.7)*rmFloat!20.0+auto_vrp.x+rmFloat!20.0;
  auto_prp.y = rmFloat!3.0; //sin(time)*rmFloat!4.0+rmFloat!4.0+auto_vrp.y+rmFloat!3.0;
  auto_prp.z = rmFloat!3.0; //cos(time*rmFloat!0.6)*rmFloat!20.0+auto_vrp.z+rmFloat!14.0;

  vuv = auto_vuv;
  vrp = auto_vrp;
  prp = auto_prp;

  // first light
  //L = Vec3(sin(rmFloat!0.0)*rmFloat!20.0, rmFloat!10.0+sin(rmFloat!0.0)*rmFloat!20.0, cos(rmFloat!0.0)*rmFloat!20.0);
  lights[1].active = true;
  lights[1].origin = Vec3(sin(worldtime)*rmFloat!20.0, rmFloat!20.0/*+sin(worldtime)*rmFloat!20.0*/, cos(worldtime)*rmFloat!20.0);
  //lights[1].origin = Vec3(rmFloat!00.0, rmFloat!(-10.0), rmFloat!00.0);
  lights[1].color.x = abs(sin(worldtime));
  lights[1].color.y = abs(cos(worldtime/3));
  lights[1].color.z = rmFloat!0;
  // second light
  lights[2].active = false;
  lights[2].origin = Vec3(rmFloat!00.0, rmFloat!(100.0), rmFloat!00.0);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared int maxTiles;
shared int curTile;


void tileRenderFunc (Tid ownerTid, uint wkid) {
  bool exit = false;
  while (!exit) {
    version(render_debug) { import core.stdc.stdio; printf("  worker %u idle...\n", wkid); }
    receive(
      (int action) {
        if (action == 0) exit = true;
      },
    );
    if (exit) break;
    // render tiles in loop
    for (;;) {
      auto tnum = atomicOp!"+="(curTile, 1)-1;
      if (tnum >= maxTiles) break; // no more tiles
      renderToTexture(tnum);
    }
    ownerTid.send(wkid, 2); // we are done
  }
  ownerTid.send(wkid, 0);
}


// ////////////////////////////////////////////////////////////////////////// //
enum ThreadCount = 4;


void renderThreadFunc () {
  Tid[ThreadCount] wkTids;
  // spawn worker threads
  foreach (uint idx; 0..ThreadCount) wkTids[idx] = spawn(&tileRenderFunc, thisTid, idx);
  // main loop
  maxTiles = tilesPerTexture; // this is constant
  for (;;) {
    if (atomicLoad(renderDie) == 1) break;
    synchronized(mutexCondCanRender) waitCondCanRender.wait();
    if (atomicLoad(renderDie) == 1) break;
    auto cft = MonoTime.currTime;
    animate();
    prepareRenderer();
    // start from tile 0 ;-)
    atomicStore(curTile, 0);
    // wake up rendering threads with "do your job" signal
    foreach (immutable idx; 0..ThreadCount) wkTids[idx].send(1);
    // wait for all rendering threads to complete
    foreach (immutable _; 0..ThreadCount) receive((uint wkid, int act) {});
    atomicStore(frameDur, cast(uint)(MonoTime.currTime-cft).total!"msecs");
    atomicStore(renderComplete, true);
  }
  // wake up rendering threads with "die" signal
  foreach (immutable idx; 0..ThreadCount) wkTids[idx].send(0);
  // wait for all rendering threads to complete
  foreach (immutable _; 0..ThreadCount) receive((uint wkid, int act) {});
  atomicStore(renderDie, 2);
}
