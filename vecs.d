module vecs;

//version = rm_use_double;

version(rm_use_double) {
  alias RmFloat = double;
} else {
  alias RmFloat = float;
}
enum rmFloat(double n) = cast(RmFloat)n;


// ////////////////////////////////////////////////////////////////////////// //
// i don't want to create a new module for this
ubyte clampToByte(T) (T n) pure nothrow @safe @nogc if (__traits(isIntegral, T)) {
  //static assert(!__traits(isUnsigned, T), "clampToByte can't process unsigned types");
  static if (__VERSION__ > 2067) pragma(inline, true);
  static if (T.sizeof == 2 || T.sizeof == 4) {
    static if (__traits(isUnsigned, T)) {
      return cast(ubyte)(n&0xff|(255-((-cast(int)(n < 256))>>24)));
    } else {
      n &= -cast(int)(n >= 0);
      return cast(ubyte)(n|((255-cast(int)n)>>31));
    }
  } else static if (T.sizeof == 1) {
    static assert(__traits(isUnsigned, T), "clampToByte: signed byte? no, really?");
    return cast(ubyte)n;
  } else {
    static assert(false, "clampToByte: integer too big");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct Vec3 {
  import std.format : FormatSpec;
  void toString (scope void delegate(const(char)[]) sink, FormatSpec!char fmt) const @trusted {
    import std.format;
    sink("Vec3(");
    sink.formatValue(x, fmt);
    sink(",");
    sink.formatValue(y, fmt);
    sink(",");
    sink.formatValue(z, fmt);
    sink(")");
  }

nothrow @safe @nogc:
  RmFloat x = 0, y = 0, z = 0;

  @property RmFloat opIndex (size_t idx) const pure {
    static if (__VERSION__ > 2067) pragma(inline, true);
    if (idx > 2) assert(0, "invalid index in Vec3 opindex");
    return (idx == 0 ? x : (idx == 1 ? y : z));
  }

  @property RmFloat opIndexAssign (RmFloat v, size_t idx) {
    static if (__VERSION__ > 2067) pragma(inline, true);
    if (idx > 2) assert(0, "invalid index in Vec3 opindex");
    final switch (idx) {
      case 0: x = v; break;
      case 1: y = v; break;
      case 2: z = v; break;
    }
    return v;
  }

  ref Vec3 normalize () {
    //pragma(inline, true);
    import std.math : sqrt;
    immutable invlen = rmFloat!1.0/sqrt(x*x+y*y+z*z);
    x *= invlen;
    y *= invlen;
    z *= invlen;
    return this;
  }

  const pure {
    static RmFloat length (RmFloat x, RmFloat y) {
      //pragma(inline, true);
      import std.math : sqrt;
      return sqrt(x*x+y*y);
    }

    static RmFloat length (RmFloat x, RmFloat y, RmFloat z) {
      //pragma(inline, true);
      import std.math : sqrt;
      return sqrt(x*x+y*y+z*z);
    }

    RmFloat length () {
      //pragma(inline, true);
      import std.math : sqrt;
      return sqrt(x*x+y*y+z*z);
      //return length(x, y, z);
    }

    Vec3 normalized () {
      //pragma(inline, true);
      import std.math : sqrt;
      immutable invlen = rmFloat!1.0/sqrt(x*x+y*y+z*z);
      return Vec3(x*invlen, y*invlen, z*invlen);
    }

    Vec3 abs () {
      //pragma(inline, true);
      import std.math : abs;
      return Vec3(abs(x), abs(y), abs(z));
    }

    Vec3 minmax(string op) (in auto ref Vec3 b) if (op == "min" || op == "max") {
      //pragma(inline, true);
      mixin("import std.algorithm : "~op~";");
      return mixin("Vec3("~op~"(x, b.x), "~op~"(y, b.y), "~op~"(z, b.z))");
    }

    Vec3 minmax(string op) (RmFloat v) if (op == "min" || op == "max") {
      //pragma(inline, true);
      mixin("import std.algorithm : "~op~";");
      return mixin("Vec3("~op~"(x, v), "~op~"(y, v), "~op~"(z, v))");
    }

    /*
    alias min = minmax!"min";
    alias max = minmax!"max";
    */

    Vec3 opBinary(string op) (in auto ref Vec3 b) if (op == "+" || op == "-" || op == "*" || op == "/") {
      static if (__VERSION__ > 2067) pragma(inline, true);
      return mixin("Vec3(x"~op~"b.x, y"~op~"b.y, z"~op~"b.z)");
    }

    Vec3 opBinary(string op) (in RmFloat b) if (op == "+" || op == "-" || op == "*" || op == "/") {
      static if (__VERSION__ > 2067) pragma(inline, true);
      return mixin("Vec3(x"~op~"b, y"~op~"b, z"~op~"b)");
    }

    // cross product (normal to plane containing a and b)
    Vec3 opBinary(string op : "%") (in auto ref Vec3 b) {
      static if (__VERSION__ > 2067) pragma(inline, true);
      return Vec3(y*b.z-z*b.y, z*b.x-x*b.z, x*b.y-y*b.x);
    }

    RmFloat dot() (in auto ref Vec3 b) {
      static if (__VERSION__ > 2067) pragma(inline, true);
      return x*b.x+y*b.y+z*b.z;
    }

    RmFloat anglev() (in auto ref Vec3 b) {
      static if (__VERSION__ > 2067) pragma(inline, true);
      import std.math : acos;
      return acos(dot(b)/(length*b.length));
    }

    // n is normal; normalized! ;-)
    Vec3 reflect() (in auto ref Vec3 n) {
      //I - 2.0 * dot(N, I) * N
      auto res = Vec3(x, y, z);
      res -= n*(rmFloat!2.0*n.dot(this));
      return res;
    }

    // things like `.xyy`
    Vec3 opDispatch(string type) ()
    if (type.length == 3 &&
        (type[0] == 'x' || type[0] == 'y' || type[0] == 'z') &&
        (type[1] == 'x' || type[1] == 'y' || type[1] == 'z') &&
        (type[2] == 'x' || type[2] == 'y' || type[2] == 'z'))
    {
      static if (__VERSION__ > 2067) pragma(inline, true);
      return mixin("Vec3("~type[0]~","~type[1]~","~type[2]~")");
    }

    /*
    auto rotx() (RmFloat angle) {
      //pragma(inline, true);
      RmFloat sine = void, cosine = void;
      asm pure nothrow @trusted @nogc {
        fld angle;
        fsincos;
        fstp [cosine];
        fstp [sine];
      }
      return Vec3(x, y*cosine-z*sine, y*sine+z*cosine);
    }

    auto roty() (RmFloat angle) {
      //pragma(inline, true);
      RmFloat sine = void, cosine = void;
      asm pure nothrow @trusted @nogc {
        fld angle;
        fsincos;
        fstp [cosine];
        fstp [sine];
      }
      return Vec3(x*cosine-z*sine, y, x*sine+z*cosine);
    }

    auto rotz() (RmFloat angle) {
      //pragma(inline, true);
      RmFloat sine = void, cosine = void;
      asm pure nothrow @trusted @nogc {
        fld angle;
        fsincos;
        fstp [cosine];
        fstp [sine];
      }
      return Vec3(x*cosine-y*sine, x*sine+y*cosine, z);
    }
    */
  }

  ref Vec3 opOpAssign(string op) (in auto ref Vec3 b) if (op == "+" || op == "-" || op == "*" || op == "/") {
    static if (__VERSION__ > 2067) pragma(inline, true);
    mixin("x"~op~"=b.x;");
    mixin("y"~op~"=b.y;");
    mixin("z"~op~"=b.z;");
    return this;
  }

  ref Vec3 opOpAssign(string op) (RmFloat b) if (op == "+" || op == "-" || op == "*" || op == "/") {
    static if (__VERSION__ > 2067) pragma(inline, true);
    mixin("x"~op~"=b;");
    mixin("y"~op~"=b;");
    mixin("z"~op~"=b;");
    return this;
  }

  //static RmFloat smoothstep (RmFloat x) pure { static if (__VERSION__ > 2067) pragma(inline, true); return x*x*(3-2*x); }
}
