// main driver
module gfxcore;

private:
import mtrender;

//version = show_frame_time;


// ////////////////////////////////////////////////////////////////////////// //
static if (__traits(compiles, () { import arsd.simpledisplay; })) {
  import arsd.simpledisplay;
} else {
  import simpledisplay;
}
import arsd.color;

__gshared SimpleWindow sdwindow;
public __gshared Image texImage;

shared static ~this () {
  destroy(texImage);
}

public enum vlWidth = 160;
public enum vlHeight = 120;
public enum scale = 3;
public enum scanlines = false;

public enum vlEffectiveWidth = vlWidth*scale;
public enum vlEffectiveHeight = vlHeight*scale;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string aftstr; // average frame time

void blitScene () {
  if (sdwindow.closed) return;
  auto painter = sdwindow.draw();
  painter.drawImage(Point(0, 0), texImage);

  //painter.outlineColor = Color.white;
  painter.outlineColor = Color.black;
  painter.drawText(Point(10, 10), aftstr);
}


public void mainLoop () {
  texImage = new Image(vlEffectiveWidth, vlEffectiveHeight);
  mtrenderInit();

  sdwindow = new SimpleWindow(vlEffectiveWidth, vlEffectiveHeight, "RayMarching demo", OpenGlOptions.no, Resizablity.fixedSize);
  blitScene();

  enum MSecsPerFrame = 1000/60; /* 60 is FPS */

  uint[8] frameTimes = 1000;

  sdwindow.eventLoop(MSecsPerFrame,
    delegate () {
      if (sdwindow.closed) return;
      if (mtrenderComplete) {
        // do average frame time
        uint aft = 0;
        foreach (immutable idx; 1..frameTimes.length) {
          aft += frameTimes.ptr[idx];
          frameTimes.ptr[idx-1] = frameTimes.ptr[idx];
        }
        frameTimes[$-1] = mtrenderFrameTime;
        aft += frameTimes[$-1];
        aft /= cast(uint)frameTimes.length;
        if (aft == 0) aft = 1000;
        {
          import std.string;
          aftstr = "%s FPS".format(1000/aft);
        }
        //version(show_frame_time) { import core.stdc.stdio; printf("%u msecs per frame\n", frameDur); }
        blitScene();
        mtrenderStartSignal();
      }
    },
    delegate (KeyEvent event) {
      if (sdwindow.closed) return;
      if (event.pressed && event.key == Key.Escape) {
        flushGui();
        sdwindow.close();
      }
    },
    delegate (MouseEvent event) {
    },
    delegate (dchar ch) {
      if (ch == ' ') mtrenderTogglePause();
    },
  );
  mtrenderShutdown();
  if (!sdwindow.closed) {
    flushGui();
    sdwindow.close();
  }
  flushGui();
}
