module objfn;

import vecs;


// lerp
Vec3 mix() (in auto ref Vec3 v0, in auto ref Vec3 v1, RmFloat t) {
  pragma(inline, true);
  immutable RmFloat t1 = rmFloat!1.0-t;
  return Vec3(
    t1*v0.x+t*v1.x,
    t1*v0.y+t*v1.y,
    t1*v0.z+t*v1.z,
  );
}


RmFloat mix() (RmFloat v0, RmFloat v1, RmFloat a) {
  pragma(inline, true);
  immutable RmFloat t1 = rmFloat!1.0-t;
  return t1*v0+t*v1;
}


// hermite interpolation
RmFloat smoothstep() (RmFloat edge0, RmFloat edge1, RmFloat x) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  immutable RmFloat t = clamp((x-edge0)/(edge1-edge0), rmFloat!0.0, rmFloat!1.0);
  return t*t*(rmFloat!3.0-rmFloat!2.0*t);
}


RmFloat clamp() (RmFloat x, RmFloat minv, RmFloat maxv) {
  //pragma(inline, true);
  import std.algorithm : min, max;
  return max(min(x, maxv), minv);
}


RmFloat fract() (RmFloat x) {
  //pragma(inline, true);
  import std.math : floor;
  return x-floor(x);
}


// is this correct?
RmFloat mod() (RmFloat x, RmFloat y) {
  //pragma(inline, true);
  import std.math : floor;
  // x minus the product of y and floor(x/y)
  return x-y*floor(x/y);
}


// ////////////////////////////////////////////////////////////////////////// //
// raymarch composition functions
// repetition
RmFloat objopRep(string objfunc) (in auto ref Vec3 pp, in auto ref Vec3 c) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  // the following MUST be `p`
  immutable auto p = Vec3(mod(p.x, c.x), mod(p.y, c.y), mod(p.z, c.z))-c*rmFloat!0.5;
  return mixin(objfunc);
}

RmFloat objopScale(string objfunc) (in auto ref Vec3 pp, RmFloat s) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  immutable auto p = pp/s;
  return mixin(objfunc)*s;
}

RmFloat objopShift(string objfunc) (in auto ref Vec3 pp, in auto ref Vec3 sv) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  immutable auto p = pp+sv;
  return mixin(objfunc);
}

// rotation/translation
/*
Vec3 objopTx(string objfunc) (in auto ref Vec3 pp, in auto ref Mat4 mt) {
  immutable auto p = m.invert*pp;
  return mixin(objfunc);
}
*/

RmFloat objopUnion() (RmFloat obj0, RmFloat obj1) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  return (obj0 < obj1 ? obj0 : obj1); // min
}

RmFloat objopSub() (RmFloat a, RmFloat b) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  return (a > -b ? a : -b);
}

// intersection
RmFloat objopInter() (RmFloat a, RmFloat b) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  return (obj0 > obj1 ? obj0 : obj1); // max
}

// returns object number
int objopUnion() (ref RmFloat dest, RmFloat obj0, RmFloat obj1, int oid0, int oid1) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  if (obj0 < obj1) {
    dest = obj0;
    return oid0;
  } else {
    dest = obj1;
    return oid1;
  }
}

// returns object number
int objopSub() (ref RmFloat dest, RmFloat a, RmFloat b, int oid0, int oid1) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  b = -b;
  if (a > b) {
    dest = a;
    return oid0;
  } else {
    dest = b;
    return oid1;
  }
}

// returns object number
int objopInter() (ref RmFloat dest, RmFloat obj0, RmFloat obj1, int oid0, int oid1) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  if (obj0 > obj1) {
    dest = obj0;
    return oid0;
  } else {
    dest = obj1;
    return oid1;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// raymarch composition functions that doesn't preserve distance
// you will probably need to decrease your step size, if you are using a raymarcher to sample this
// the displacement example below is using sin(20*p.x)*sin(20*p.y)*sin(20*p.z) as displacement pattern
/*
RmFloat objopDisplace(string objfunc) (in auto ref Vec3 p) {
  RmFloat d1 = mixin(objfunc);
  RmFloat d2 = displacement(p);
  return d1+d2;
}
*/


// exponential smooth min (k = 32);
RmFloat sminExp (RmFloat a, RmFloat b, RmFloat k) {
  import std.math : exp, log;
  RmFloat res = exp(-k*a)+exp(-k*b);
  return -log(res)/k;
}

// power smooth min (k = 8);
RmFloat sminPow (RmFloat a, RmFloat b, RmFloat k) {
  import std.math : pow;
  a = pow(a, k);
  b = pow(b, k);
  return pow((a*b)/(a+b), rmFloat!1.0/k);
}

// polynomial smooth min (k = 0.1);
/*
RmFloat sminPoly (RmFloat a, RmFloat b, RmFloat k) {
  import std.math : clamp, exp, log;
  RmFloat h = clamp(rmFloat!0.5+rmFloat!0.5*(b-a)/k, rmFloat!0.0, rmFloat!1.0);
  return mix(b, a, h)-k*h*(rmFloat!1.0-h);
}
*/

RmFloat objopBlend(alias sminfn, string objfunc0, string objfunc1) (in auto ref Vec3 p) {
  RmFloat d1 = mixin(objfunc0);
  RmFloat d2 = mixin(objfunc1);
  return sminfn(d1, d2);
}


RmFloat objopTwist (string objfunc) (in auto ref Vec3 pp) {
  import std.math : cos, sin;
  immutable RmFloat c = cos(rmFloat!10.0*pp.z+rmFloat!10.0);
  immutable RmFloat s = sin(rmFloat!10.0*pp.z+rmFloat!10.0);
  //immutable auto m = Mat2(c, -s, s, c);
  // Mat2
  /*
  RmFloat[2][2] mm = [
    [c, -s],
    [s,  c]
  ];
  */
  // multiple matrix2 by vector2
  //immutable auto p = Vec3(m*p.xz, p.y);
  /*
  RmFloat v2x = mm.ptr[0].ptr[0]*pp.x+mm.ptr[0].ptr[1]*pp.z;
  RmFloat v2y = mm.ptr[1].ptr[0]*pp.x+mm.ptr[1].ptr[1]*pp.z;
  */
  RmFloat v2x = c*pp.x-s*pp.y;
  RmFloat v2y = s*pp.x+c*pp.y;
  immutable auto p = Vec3(v2x, v2y, pp.z);
  return mixin(objfunc);
}


// ////////////////////////////////////////////////////////////////////////// //
import std.algorithm : maxf = max, minf = min;

// raymarch object functions
// as our functions are very simple, use strings and `mixin` to simulate inlining ;-)
enum floorHeight = rmFloat!2.0;
enum objfuncFloor = q{(p.y+floorHeight)};

// n must be normalized (invalid formula, broken by k8)
static immutable planeN = Vec3(rmFloat!0.0, rmFloat!0.0, rmFloat!0.0);
enum planeD = rmFloat!12.0;
enum objfuncPlane = q{(p.dot(planeN)+planeD)};

enum torusOuterRadius = rmFloat!2.4;
enum torusRadius = rmFloat!0.6;
enum objfuncTorus = q{(Vec3.length(p.length-torusOuterRadius, p.z)-torusRadius)};

// sphere, signed
enum shpereRadius = rmFloat!1.9;
enum objfuncSphere = q{(p.length-shpereRadius)};

// round box, unsigned
static immutable roundboxSize = Vec3(rmFloat!2.0, rmFloat!0.7, rmFloat!2.0);
enum roundboxRadius = rmFloat!0.2;
enum objfuncRoundbox = q{((p.abs-roundboxSize).minmax!"max"(rmFloat!0.0).length-roundboxRadius)};

// box, signed (interestingly, this is more complex than round box ;-)
static immutable boxSize = Vec3(rmFloat!2.0, rmFloat!0.7, rmFloat!2.0);
__gshared Vec3 boxTemp;
enum objfuncBox = q{(boxTemp = p.abs-boxSize, minf(maxf(boxTemp.x, maxf(boxTemp.y, boxTemp.z)), rmFloat!0.0)+boxTemp.minmax!"max"(rmFloat!0.0).length)};

// box, unsigned
static immutable boxuSize = Vec3(rmFloat!2.0, rmFloat!0.7, rmFloat!2.0);
enum objfuncBoxu = q{((p.abs-boxuSize).minmax!"max"(rmFloat!0.0).length)};

// cylinder, signed
static immutable cylParams = Vec3(rmFloat!0.0, rmFloat!0.0, rmFloat!0.6); // x, y, width
enum objfuncCyl = q{(Vec3.length(p.x-cylParams.x, p.z-cylParams.y)-cylParams.z)};


