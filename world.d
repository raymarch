module world;

private:
import objfn;
import vecs;


// ////////////////////////////////////////////////////////////////////////// //
// object ids:
enum ObjId : int {
  floor,
  roundbox,
  roundboxInside,
  torus,
  box,
}


// ////////////////////////////////////////////////////////////////////////// //
public RmFloat mapWorld() (in auto ref Vec3 p, ref int obj) {
  //static if (__VERSION__ > 2068) pragma(inline, true); // yep, 2.068
  // we need object number to distinguish floor from other objects
  // we can paint objects too by getting their numbers ;-)
  // subtraction always gives us roundbox
  /*
  obj = ObjId.roundbox;
  //obj = ObjId.floor;
  RmFloat res = void;
  //obj = objopSub(res, objopShift!(objfuncRoundbox)(p, Vec3(rmFloat!2.0, rmFloat!0.0, rmFloat!0.0)), mixin(objfuncSphere), ObjId.roundbox, ObjId.roundboxInside);
  //res = objopShift!(objfuncRoundbox)(p, Vec3(rmFloat!2.0, rmFloat!0.0, rmFloat!0.0));
  res = mixin(objfuncRoundbox);
  //obj = objopUnion(res, mixin(objfuncTorus), res, ObjId.torus, obj);
  obj = objopSub(res, res, mixin(objfuncTorus), obj, ObjId.torus);
  //obj = objopUnion(res, objopShift!(objfuncTorus)(p, Vec3(rmFloat!0.0, -rmFloat!2.0, rmFloat!0.0)), res, ObjId.torus, obj);
  //!obj = objopUnion(res, objopTwist!objfuncTorus(p), res, ObjId.torus, obj);
  obj = objopUnion(res, mixin(objfuncFloor), res, ObjId.floor, obj);
  */
  RmFloat res = void;
  obj = ObjId.roundbox;
  obj = objopSub(res, objopShift!(objfuncRoundbox)(p, Vec3(rmFloat!2.0, rmFloat!0.0, rmFloat!0.0)), mixin(objfuncSphere), ObjId.roundbox, ObjId.roundboxInside);
  obj = objopUnion(res, mixin(objfuncTorus), res, ObjId.torus, obj);
  obj = objopUnion(res, mixin(objfuncFloor), res, ObjId.floor, obj);
  return res;
}


public RmFloat mapWorld() (in auto ref Vec3 p) {
  static if (__VERSION__ > 2067) pragma(inline, true);
  int obj;
  return mapWorld(p, obj);
}


// ////////////////////////////////////////////////////////////////////////// //
// floor color (checkerboard)
Vec3 floorColor() (in auto ref Vec3 p) @nogc {
  static if (__VERSION__ > 2067) pragma(inline, true);
  Vec3 res = void;
  if (fract(p.x*rmFloat!0.2) > rmFloat!0.2) {
    if (fract(p.z*rmFloat!0.2) > rmFloat!0.2) {
      res.x = rmFloat!0.0;
      res.y = rmFloat!0.1;
      res.z = rmFloat!0.2;
    } else {
      res.x = res.y = res.z = rmFloat!1.0;
    }
  } else {
    if (fract(p.z*rmFloat!0.2) > rmFloat!0.2) {
      res.x = res.y = res.z = rmFloat!1.0;
    } else {
      res.x = rmFloat!0.3;
      res.y = rmFloat!0.0;
      res.z = rmFloat!0.0;
    }
  }
  return res;
}


// get primitive color
public Vec3 getObjColor() (in auto ref Vec3 p, int obj) {
  switch (obj) {
    case ObjId.floor:
      return floorColor(p);
    case ObjId.roundbox:
      return Vec3(rmFloat!0.0, rmFloat!0.8, rmFloat!0.0);
    case ObjId.roundboxInside:
      return Vec3(rmFloat!0.0, rmFloat!0.3, rmFloat!0.0);
    case ObjId.torus:
      return Vec3(rmFloat!0.6, rmFloat!0.6, rmFloat!0.8);
    default:
      // RED
      { import core.stdc.stdio; printf("WTF?!\n"); }
      return Vec3(rmFloat!1.0, 0, 0);
  }
}
