module raytracer;

private:
import objfn;
import vecs;
import world;


// ////////////////////////////////////////////////////////////////////////// //
public struct LightInfo {
  bool active;
  Vec3 origin;
  Vec3 color;
}


// ////////////////////////////////////////////////////////////////////////// //
// raymarch global vars
public __gshared RmFloat worldtime = 0; // seconds
public __gshared Vec3 vrp; // view reference point
public __gshared Vec3 vuv; // view up vector
public __gshared Vec3 prp; // camera position


immutable Vec3 eps = Vec3(rmFloat!0.001, rmFloat!0.0, rmFloat!0.0);
immutable Vec3 exyy = eps.xyy;
immutable Vec3 eyxy = eps.yxy;
immutable Vec3 eyyx = eps.yyx;


// ////////////////////////////////////////////////////////////////////////// //
// main raycaster routine
struct RayCastRes {
nothrow @safe @nogc:
  int obj; // obj id or -1 if we doesn't hit anything
  // the following fields are undefined if obj is -1
  RmFloat objdist; // in teh world, from ray origin to nearest object
  RmFloat dist; // how much we traveled?
  Vec3 hitp; // hitpoint

  @property bool hit () const pure { pragma(inline, true); return (obj >= 0); }
}


void castRay() (ref RayCastRes res, in auto ref Vec3 ro, in auto ref Vec3 rd, RmFloat tmin=rmFloat!1.0, RmFloat tmax=rmFloat!20.0) {
  enum MaxSteps = 1024; // arbitrary limit, it will be culled by precision
  enum precis = rmFloat!0.002;
  RmFloat t = tmin;
  int obj = 0;
  Vec3 rp = void;
  RmFloat odst = void;
  int i = 0;
  for (; i < MaxSteps; ++i) {
    rp = ro+rd*t;
    odst = mapWorld(rp, obj);
    if (odst < precis || t > tmax) break;
    t += odst;
  }
  //if (i >= MaxSteps) t -= odst;
  //if (i >= MaxSteps) t = tmax+rmFloat!1;
  if (i >= MaxSteps || t > tmax) {
    res.obj = -1;
    //res.dist = tmax+rmFloat!1; // just in case
  } else {
    res.obj = (obj < 0 ? 0 : obj);
    res.objdist = odst;
    res.dist = t;
    res.hitp = rp;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// calc normal to the nearest hitpoint
Vec3 calcNormal() (in auto ref Vec3 pos) {
  return Vec3(
    mapWorld(pos+exyy)-mapWorld(pos-exyy),
    mapWorld(pos+eyxy)-mapWorld(pos-eyxy),
    mapWorld(pos+eyyx)-mapWorld(pos-eyyx)
  ).normalize;
}


// calc ambient occlusion
RmFloat calcAO() (in auto ref Vec3 pos, in auto ref Vec3 nor) {
  RmFloat occ = rmFloat!0.0;
  RmFloat sca = rmFloat!1.0;
  for (int i = 0; i < 5; ++i) {
    RmFloat hr = rmFloat!0.01+rmFloat!0.12*cast(RmFloat)i/rmFloat!4.0;
    Vec3 aopos = nor*hr+pos;
    RmFloat dd = mapWorld(aopos);
    occ += -(dd-hr)*sca;
    sca *= rmFloat!0.95;
  }
  return clamp(rmFloat!1.0-rmFloat!3.0*occ, rmFloat!0.0, rmFloat!1.0);
}


// trace hard shadow
RmFloat shadow() (in auto ref Vec3 ro, in auto ref Vec3 rd, RmFloat mint, RmFloat tmax) {
  import std.algorithm : min;
  RmFloat res = rmFloat!1.0;
  RmFloat t = mint;
  for (int i = 0; i < 16; ++i) {
    RmFloat h = mapWorld(ro+rd*t);
    t += clamp(h, rmFloat!0.02, rmFloat!0.10);
    if (h < rmFloat!0.001 || t > tmax) return rmFloat!0.0;
  }
  return rmFloat!1.0;
}


// trace soft shadow
RmFloat softshadow() (in auto ref Vec3 ro, in auto ref Vec3 rd, RmFloat mint, RmFloat tmax, RmFloat k=rmFloat!8.0) {
  import std.algorithm : min;
  RmFloat res = rmFloat!1.0;
  RmFloat t = mint;
  for (int i = 0; i < 16; ++i) {
    RmFloat h = mapWorld(ro+rd*t);
    res = min(res, k*h/t);
    t += clamp(h, rmFloat!0.02, rmFloat!0.10);
    if (h < rmFloat!0.001 || t > tmax) break;
  }
  return clamp(res, rmFloat!0.0, rmFloat!1.0);
}


// ////////////////////////////////////////////////////////////////////////// //
// compute screen space derivatives of positions analytically without dPdx()
void calcDpDxy() (in auto ref Vec3 ro, in auto ref Vec3 rd, in auto ref Vec3 rdx, in auto ref Vec3 rdy, RmFloat t, in auto ref Vec3 nor, ref Vec3 dpdx, ref Vec3 dpdy) {
  dpdx = (rdx*rd.dot(nor)/rdx.dot(nor)-rd)*t;
  dpdy = (rdy*rd.dot(nor)/rdy.dot(nor)-rd)*t;
}

// ////////////////////////////////////////////////////////////////////////// //
// raymarching
// light 0: fog
public void raymarch() (/*RmFloat x, RmFloat y,*/ in auto ref Vec3 rd, const(LightInfo)[] lights, ref Vec3 color, in auto ref Vec3 rdx, in auto ref Vec3 rdy) @nogc {
  enum maxd = rmFloat!100.0; // max depth

  RayCastRes rcres = void;

  castRay(rcres, prp, rd, rmFloat!1.0, maxd);

  // did we hit something?
  if (rcres.hit) {
    import std.math : exp, pow;

    Vec3 nor = calcNormal(rcres.hitp);
    Vec3 refl = rd.reflect(nor);

    Vec3 dposdx = void, dposdy = void;
    calcDpDxy(prp, rd, rdx, rdy, rcres.dist, nor, dposdx, dposdy);
    // get primitive color
    auto col = getObjColor(rcres.hitp, rcres.obj);

    // old
    //vec2 uv = textureMapping( pos, oid );
    //Vec3 sur = texture( sampler, uv );
    // new
    //vec2 (uv,duvdx,duvdy) = textureMapping( pos, dposdx, dposdy, oid );
    //Vec3 sur = textureGrad( sampler, uv, dudvx, dudvy );

    // lighting
    RmFloat occ = calcAO(rcres.hitp, nor);
    foreach (immutable lidx; 1..lights.length) {
      auto lt = lights.ptr+lidx;
      if (lt.active) {
        //Vec3 lig = Vec3(-rmFloat!0.6, rmFloat!0.7, -rmFloat!0.5).normalize;
        Vec3 lig = lt.origin.normalized;
        RmFloat amb = clamp(rmFloat!0.5+rmFloat!0.5*nor.y, rmFloat!0.0, rmFloat!1.0);
        RmFloat dif = clamp(nor.dot(lig), rmFloat!0.0, rmFloat!1.0);
        RmFloat bac = clamp(nor.dot(Vec3(-lig.x, rmFloat!0.0, -lig.z).normalize), rmFloat!0.0, rmFloat!1.0)*clamp(rmFloat!1.0-rcres.hitp.y, rmFloat!0.0, rmFloat!1.0);
        RmFloat dom = smoothstep(-rmFloat!0.1, rmFloat!0.1, refl.y);
        RmFloat fre = pow(clamp(rmFloat!1.0+nor.dot(rd), rmFloat!0.0, rmFloat!1.0), rmFloat!2.0);
        RmFloat spe = pow(clamp(refl.dot(lig), rmFloat!0.0, rmFloat!1.0), rmFloat!16.0);

        dif *= softshadow(rcres.hitp, lig, rmFloat!0.02, maxd);
        dom *= softshadow(rcres.hitp, refl, rmFloat!0.02, maxd);

        // light color
        //immutable lc = Vec3(rmFloat!1.00, rmFloat!0.85, rmFloat!0.55);
        //immutable lc = Vec3(rmFloat!0.0, rmFloat!0.0, rmFloat!0.0);
        // calculate light
        auto lc = &lt.color;
        Vec3 lin = Vec3(rmFloat!0.0, rmFloat!0.0, rmFloat!0.0);
        lin += (*lc)*(rmFloat!1.20*dif);
        lin += (*lc)*(rmFloat!1.20*spe)*dif;
        // ambient occlusion
        lin += Vec3(rmFloat!0.50, rmFloat!0.70, rmFloat!1.00)*(rmFloat!0.20*amb)*occ;
        lin += Vec3(rmFloat!0.50, rmFloat!0.70, rmFloat!1.00)*(rmFloat!0.30*dom)*occ;
        lin += Vec3(rmFloat!0.25, rmFloat!0.25, rmFloat!0.25)*(rmFloat!0.30*bac)*occ;
        lin += Vec3(rmFloat!1.00, rmFloat!1.00, rmFloat!1.00)*(rmFloat!0.40*fre)*occ;
        col *= lin;
      }
    }
    if (lights.length && lights.ptr[0].active) {
      // fog
      col = mix(col, lights.ptr[0].color, rmFloat!1.0-exp(-rmFloat!0.002*rcres.dist*rcres.dist));
    }
    // done
    color = col;
  } else {
    // background color
    color = Vec3(rmFloat!0.7, rmFloat!0.9, rmFloat!1.0)+rd.y*rmFloat!0.8;
  }
}
